﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyIpc.Messaging;

namespace MLEngine.Entities
{
    class TypedTinyBus
    {
        public TinyMessageBus tinyMessageBus;
        public string channelName;

        public TypedTinyBus(string channelName)
        {
            this.channelName = channelName;
            tinyMessageBus = new TinyMessageBus(channelName);
        }

        public void Start()
        {
            tinyMessageBus.MessageReceived += TinyMessageBus_MessageReceived;
        }

        public void Stop()
        {
            tinyMessageBus.MessageReceived -= TinyMessageBus_MessageReceived;
            tinyMessageBus.Dispose();
        }

        public async Task SendMessageAsync(string message)
        {
            await tinyMessageBus.PublishAsync(Encoding.UTF8.GetBytes(message));
        }

        private void TinyMessageBus_MessageReceived(object sender, TinyMessageReceivedEventArgs e)
        {
            NewMessageRecived(channelName, Encoding.UTF8.GetString(e.Message));
        }

        public event Action<string, string> NewMessageRecived;
    }
}
